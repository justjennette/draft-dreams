import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise'
import 'rxjs/add/operator/map'
import { CookieService } from 'ngx-cookie';
// import { ResponseContentType } from '@angular/http';
import { Router } from '@angular/router';

import { Fantasy, Player, Draft, User, Rank, Item } from './shared'
import { environment } from '../environments/environment'

declare var window

@Injectable() 
export class API {

  draft: AngularFirestoreCollection<Draft>
  draftIndex: AngularFirestoreCollection<Draft>

  rank: AngularFirestoreCollection<Rank>

  constructor(private http: HttpClient, private cookie: CookieService, private router: Router, private afs: AngularFirestore) {
    if(window.location.pathname !== '/login') {
      if(!this.loggedIn()) this.router.navigateByUrl('/login')
    }
  }

  loggedIn(): boolean {
    let userId = this.cookie.get('user')
    if(!userId) return false
    return true
  }

  clearCookie() {
    this.cookie.remove('user')
  }

  userId() {
    return this.getCookie('user')
  }

  getAllDrafts(): Observable<Draft> {
    return this.afs.collection('draftIndex', ref => ref
      .where('user', '==', this.userId())
    ).valueChanges()
  }

  initDraft(): Observable<Draft> {
    this.draft = this.afs.collection<Draft>('draft')
    this.draftIndex = this.afs.collection<Draft>('draftIndex')
    return this.draft.valueChanges()
  }

  initRanks(): Observable<Draft> {
    // this.rank = this.afs.collection<Draft>('playerRank')
    this.rank = this.afs.collection('playerRank', ref => ref
      // .where('id', '==', details.id)
      .orderBy('rank')
    )
    return this.rank.valueChanges()
  }

  updateDraft(details): Promise<any> {
    details.userIndex = +!details.userIndex // switch between 0 and 1 hack
    return this.draft.doc(details.id).update(details)
  }

  addDraft(details) {
    this.draft.add(Object.assign({}, details)).then((docRef) => {
      let draftId = docRef.id
      this.draft.doc(draftId).update({
        id: draftId
      })
      this.addDraftIndex(draftId, details.users)
    })
  }

  addDraftIndex(draftIndex, users) {
    let counter = 0
    for(let user of users) {
      let userDetails = {
        user: user,
        id: draftIndex
      }
      this.draftIndex.add(userDetails).then( (values) => {

        if(counter === users.length)
          this.router.navigateByUrl('/draft/' + draftIndex)

        counter += 1
      })
    }
  }

  getRanks(details): Observable<Rank> {
    return this.afs.collection('rank', ref => ref
      .where('position', '==', 'QB')
    ).valueChanges()
  }

  getDraft(details): Observable<Draft> {
    return this.afs.collection('draft', ref => ref
      .where('id', '==', details.id)
    ).valueChanges()
  }

  getCookie(name: string) {
    return this.cookie.get(name)
  }

  setCookie(name: string, value: string) {
    this.cookie.put(name, value)
    this.getCookie(name) // debug
  }

  get(file: string, path?: string): Promise<any> {

    if(file && !path) path = file

    if(this.hasLocal(path)) {
      console.debug('get()', this.getLocal(path))
      let getPromise = this.getLocal(path)
      if(getPromise) return getPromise
    }

    let url = environment.urls.base + '/' + file + environment.urls.ext + '&week=' + new Fantasy().week

    return this.call(url).then(values =>  this.saveLocal(file, path, values[path]))
  }

  hasLocal(path: string): boolean {
    let storage = localStorage.getItem(path)
    if(storage) return true
    return false
  }

  saveLocal(file: string, path?: string, values?: any): Promise<any> {
    if(!values) return Promise.resolve()
    if(!path) path = file
    localStorage.setItem( path, JSON.stringify(values) )
    values[path] = values
    return Promise.resolve().then(function() { return values })
  }

  getLocal(path: string): Promise<any> {
    let item = []
    let storage = localStorage.getItem(path)
    if(storage) {
      let data = JSON.parse(storage)
      console.debug('getLocal', data)
      if(data) {
        item[path] = data
        // return Promise.resolve().then(function() { return item })
      }
    }
    return Promise.resolve().then(function() { return item })
  }

  getImage(player): string {

    let cachedImage = localStorage.getItem(player.esbid)
    if(cachedImage) {
      player.image = cachedImage
      return ''
    }

    this.http
        .get('/download/' + player.esbid + '.png')
        .subscribe(
        (response) => {

          var blob = new Blob([response], {type: 'application/png'});

          var reader = new window.FileReader()
          reader.readAsDataURL(blob)
          reader.onloadend = function() {
            player.image = reader.result // base64data
            localStorage.setItem(player.esbid, reader.result)
            return player.image
            // console.debug('adding player ' + player.name)
            // this.playersCollection.add(player)
          }

        },
        // (err:any) => return '',
        () => console.log('Complete')
      );
  }

  debug(values) {
    console.debug('api.service debug', values)
  }

  delete(collection: AngularFirestoreCollection<any>) {
    collection.snapshotChanges().subscribe(values => function(values) {
      for(let d in values.docs) {
        let id = values.docs[d].payload.doc['id']
        console.debug('payload ID', id)
      }
    })
  }

  call(url: string, local?: boolean, params?: Array<object>): Promise<any> {
    
    // const url = `${environment.urls.base}details${environment.urls.ext}` // alternate way to write
    // let url = path
    
    // if(local) url = environment.urls.base + path + environment.urls.ext

    console.debug('api.service call', url)

    return this.http.get(url)
            .toPromise()
            .then( response => response )
            .catch(this.handleError)
  }

  private handleError(err: any): Promise<any> {
    console.error('Error occurred', err)
    return Promise.reject(err.message || err)
  }
}