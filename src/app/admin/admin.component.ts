import { Component } from '@angular/core';
// import { Headers, Http } from '@angular/http'
import { HttpClient } from '@angular/common/http'
import 'rxjs/add/operator/map';
import { DomSanitizer } from '@angular/platform-browser';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';

import { API } from '../api.service'
import { Player, Stats, Item, Fantasy } from '../shared'
import { Observable } from 'rxjs';
// import { ResponseContentType } from '@angular/http';

declare var window

@Component({
  selector: 'admin',
  templateUrl: './admin.component.html',
  providers: [ API ]
})
export class Admin {

  status: string
  loading = false
  error: string
  details = { week: new Fantasy().week }

  options = {
    single: false,
    test: false
  }

  playersCollection: AngularFirestoreCollection<any>
  players: Observable<any[]>

  statsCollection: AngularFirestoreCollection<Stats>
  rankCollection: AngularFirestoreCollection<Item>
  researchCollection: AngularFirestoreCollection<Item>

  constructor(private api: API, private afs: AngularFirestore, private http: HttpClient, public sanitizer: DomSanitizer) {
    this.playersCollection = afs.collection<any>('players')
    this.statsCollection =   afs.collection<any>('playerStats')
    this.rankCollection =   afs.collection<any>('playerRank')
    this.researchCollection =   afs.collection<any>('playerResearch')

    this.players = this.playersCollection.valueChanges()

  }

  private run(path: string, data: any) {

    let _this = this

    if(!data[path]) {
      this.loading = false
      this.error = 'No data found'
      return
    }

    let total = data[path].length
    
    if(!total) {
      data[path] = Object.entries(data[path])
      total = data[path].length
    }
    
    if(total === 0 || !total) {
      this.loading = false
      this.error = 'No data found'
      return
    }

    if(this.options.test) {
      this.status = 'Test mode only'
      this.loop(path, data)
      return
    }

    if(this.options.single) {
      this.status = 'Single check for ' + path
      this.loop(path, data)
      return
    }

    for (var p = 0; p < total; p++) {
     (function(ind) {
         setTimeout(function(){
            _this.status = (ind + 1) + ' of ' + total + ' completed'
            _this.loop(path, data, ind)
            if(p === total) _this.loading = false
         }, 1000 + (1000 * ind));
     })(p)
    }
    
  }

  private loop(path, data, ind?: number) { // path, data, index

    if(!ind) ind = 0

    let arr = data[path][ind]

    if(this.options.test) {
      console.debug(arr)
      return
    }

    switch (path) {
      case 'playerStats':
        let key = arr[0]
        let values = { stats: arr[1] }
        this.statsCollection.add(values).then((docRef) => {
          this.statsCollection.doc(docRef.id).update({
            playerId: key,
            id: docRef.id,
            week: this.details.week
          })
        })
        break;

      case 'players': 
        this.addPlayer(arr)
        break;

      case 'editorweekranks':
        let rank = {
          id: arr.id,
          name: arr.firstName + ' ' + arr.lastName,
          position: arr.position,
          week: this.details.week,
          rank: Number(arr.rank)
        }
        this.addRank(rank)
        break;
      
      default:
        break;
    }
  }

  addRank(data) {
    this.rankCollection.add(data)
  }

  addPlayer(player) {

    return this.http
        .get('/download/' + player.esbid + '.png')
        .subscribe(
        (response) => {

          var blob = new Blob([response], {type: 'application/png'});

          var reader = new window.FileReader()
          reader.readAsDataURL(blob)
          reader.onloadend = function() {
            player.image = reader.result // base64data
            console.debug('adding player')
            this.playersCollection.add(player)
          }

        },
        (err:any) => this.playersCollection.add(player),
        () => console.log('Complete')
      );
  }

  rankings(data) {
    // call('rank', 'players') // old method
  }

  get(path: string) {
    let url = 'http://api.fantasy.nfl.com/v1/players/'+ path +'?format=json&count=9999'
    // if(params) url += '&' + params

    // standard default
    let positions = new Fantasy().SPOS

    switch(path) {

      case 'editorweekranks':
        this.api.delete(this.rankCollection)
        // for(let p in positions) {
        //   this.api.call(url + '&position=' + positions[p]).then(data => this.run('players', data) )
        // }
      break

      default:
        this.api.call(url).then(data => this.api.debug(data))
      break
    }
  }

    // TODO: add in editor/player ranking
    // TODO: Auto delete old rankings per week (cron?)
    // if(path === 'rank') {
    //   this.rankCollection.snapshotChanges().subscribe(data => {
    //     console.debug(data)
    //     return data.map(a => {
    //       // const id = a.payload.doc.id
    //       _this.afs.doc('playerRank/' + a.payload.doc.id).delete()
    //       // a.payload.doc
    //     })

    //   })
    // }

}