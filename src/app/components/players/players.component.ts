import { Component, OnInit } from '@angular/core';
// import { Observable } from 'rxjs/Observable';

import { API } from '../../api.service'
import { Fantasy, Player, Stats, Rank } from '../../shared'

@Component({
  selector: 'app-players',
  templateUrl: './players.component.html'
})
export class PlayersComponent implements OnInit {

  players: Player[]
  stats: Stats[]
  ranks: Rank[]

  loading = true
  details = {}

  currentWeek = new Fantasy().week

  constructor(public api: API) { }

  // loadStats(data) {
  //   // console.debug(data)
  //   this.loading = false
  //   // this.ready()
  //   this.getRanks()
  // }

  loadRanks(data) {
    this.api.saveLocal('rank', data).then(data => this.ready)
  }

  compare(a:any, b:any) {
    let field = 'rank'
    if (a[field] < b[field]) {
      return -1
    } else if (a[field] > b[field]) {
      return 1
    } else {
      return 0
    }
  }

  mergeRanks() { // add in the ranking number to each player
    for(let p in this.players) {
      let player:Player = this.players[p]
      for(let r in this.ranks) {
        let rank:Rank = this.ranks[r]
        if(rank['id'] === player['id']) {
          this.players[p]['rank'] = rank['rank']
        }
      }
      if(!this.players[p]['rank']) this.players[p]['rank'] = 9999 // default rank
    }

    console.debug(this.players)
    
    if(!this.players) {
      this.ready()
      return
    }
    this.players.sort(this.compare) // assigned ranked players
    this.ready()
  }

  ready() {
    this.loading = false
    console.debug('players ready!!', this.players)
  }

  getPlayers() {
    this.loading = true
    this.api.get('players/stats', 'players')
        // .then(data => console.debug(data))
        .then(data => this.players = data['players'])
        .then(this.getRanks.bind(this))
  }

  // getStats() {
  //   this.api.call('playerStats').then(data => this.loadStats(data))
  // }

  getRanks() {
    // Check local copy first
    if(this.api.hasLocal('rank')) {
      this.api.getLocal('rank')
        .then( rankData => { this.ranks = rankData['rank'] } )
        .then( this.mergeRanks.bind(this) )
      return
    }

    // No local copy, init DB and get data
    this.api.initRanks()
      .subscribe( rankData => this.api.saveLocal('rank', 'rank', rankData)
      .then( rankData => { this.ranks = rankData } )
      .then( this.mergeRanks.bind(this) ) )
  }

  ngOnInit() {
    this.getPlayers()
  }

}
