import { Component, OnInit } from '@angular/core';
import { RouterModule, ActivatedRoute, ParamMap, Routes, Router } from '@angular/router'

import { API } from '../../api.service'
import { Draft, Player } from '../../shared'

import { PlayersComponent } from '../players/players.component'

@Component({
  selector: 'app-draft',
  templateUrl: './draft.component.html'
})
export class DraftComponent extends PlayersComponent implements OnInit {

  draft: Draft

  seconds = 60
  timer

  details = {
    id: '',
    invite: '',
    accepted: false,
    userIndex: 0,
    users: [],
    round: 1,
    currentUser: '',
    players: []
  }

  constructor(public api: API, private router: Router, private route: ActivatedRoute) {

    super(api)
    
    if(route.snapshot.params['id']) this.details.id = route.snapshot.params['id']
    if(route.snapshot.params['invite']) this.details.invite = route.snapshot.params['invite']

  }

  init(data) {
    
    // Invite check
    if(this.details.invite && data.length === 0) {
      this.details.users = [ this.api.userId(), this.details.invite ]
      this.api.addDraft(this.details)
      return
    }

    // Load draft
    this.updateDraft()
  }

  getDetails() {
    return this.details
  }

  updateDraft() {
    // this.loading = true
    // if(!this.timer) this.timer = setInterval(this.updateDraft.bind(this), 1000 * this.seconds)
    this.api.getDraft(this.details).subscribe( values => this.loadDraft(values) )
  }

  loadDraft(data) {
    if(data.length > 0) {
      this.details = data[0]
      this.details.currentUser = this.api.userId()
      // this.api.get('players').then(data => this.loadPlayers(data))
      if(!this.players) this.getPlayers() // inherited from players.component
      return
    }
    this.router.navigateByUrl('/')
  }

  ready() {
    console.debug('draft override!')
    this.loading = false
    setTimeout(this.updateDraft.bind(this), 1000 * this.seconds)
  }

  updateDetails(data) {
    this.details = data
  }

  add(player: Player) {

    if(!player) return

    this.details.players.push( { player: player.id, user: this.details.users[this.details.userIndex], round: this.details.round } )
    this.details.round += 1

    // save details to db
    this.api.updateDraft(this.details).then(values => this.updateDetails(values))
    
    return this.details
  }

  ngOnInit() {
    // this.getPlayers()
    this.api.initDraft().subscribe( values => this.init(values) )
  }

}
