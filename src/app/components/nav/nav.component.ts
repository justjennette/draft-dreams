import { Component, OnInit } from '@angular/core';

import { API } from '../../api.service'

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html'
})
export class NavComponent implements OnInit {

  loggedIn = false
  isCollapsed = false

  constructor(private api: API) { }

  ngOnInit() {
    this.loggedIn = this.api.loggedIn()
  }

}
