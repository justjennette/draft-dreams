import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireDatabase } from 'angularfire2/database';
import { Router, ActivatedRoute } from '@angular/router';

import { API } from '../../api.service'
import { NavComponent } from '../nav/nav.component'
import { User } from '../../shared'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  loading = 0
  isLoading = false
  mode = 'login'

  status: string
  error:  string

  registered = false
  loggedIn = false

  login: User
  register: User

  user: User
  usersCollection: AngularFirestoreCollection<User>
  users: Observable<User[]>

  doLogin() {

    this.loading = 10
    this.error = ''
    this.status = ''

    if(!this.login.user || !this.login.pass) return

    this.afs.collection('users', ref => ref
      .where('user', '==', this.login.user)
      .where('pass', '==', this.login.pass) 
    ).valueChanges().subscribe( values => this.loadLogin(values) )

  }

  loadLogin(data) {
    var details = data[0]
    if(!details) {
      // user not found
      this.error = 'User not found'
      return
    }
    this.api.setCookie('user', details.id)
    this.nav.loggedIn = true
    this.status = 'Logged in, please wait...'
    this.router.navigateByUrl('/dashboard')
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router, 
    private nav: NavComponent,
    private api: API, 
    private afs: AngularFirestore, 
    private af: AngularFireDatabase) {
      this.init()
  }

  init() {
    this.register = new User()
    this.login = new User()
    if(this.router.url == '/logout') {
      this.nav.loggedIn = false
      this.api.clearCookie()
      return
    }
    this.usersCollection = this.afs.collection<User>('users')
    this.users = this.usersCollection.valueChanges()
    if(this.api.loggedIn()) console.debug('logged in')
  }

  doRegister() {
    this.status = 'Registered!'
    this.registered = true
    this.usersCollection.add(Object.assign({},this.register)).then((docRef) => {
      this.usersCollection.doc(docRef.id).update({
        id: docRef.id
      })
    })
  }

  setMode(what) {
    this.mode = what
  }

  ngOnInit() {}

}
