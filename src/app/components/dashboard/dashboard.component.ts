import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { API } from '../../api.service'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent {

  drafts = []

  settings = {
    url: window.location.origin + '/invite/',
    public: true,
    uid: ''
  }

  constructor(private api: API) {
    this.settings.uid = api.userId()
    this.api.getAllDrafts().subscribe( values => this.loadDrafts(values) )
  }

  loadDrafts(data) {
    // if(data.length < 1) return
    this.drafts = data
  }

}
