import { Component, Injectable, Input, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap'

import { Draft, Player, Positions } from '../../shared'
import { DraftComponent } from '../draft/draft.component'

import { ArraySortPipe } from '../../tools/pipe';

@Component({
  selector: 'list',
  templateUrl: './list.component.html'
})

export class ListComponent implements OnInit {

  id: number
  source: string
  destination: string

  searchValue: string
  position: string

  selectedPlayer: Player
  currentPosition = 0
  isSearching = false

  positions:Positions

  @Input() players
  @Input() details

  // how many rows to display to user per page
  limit = 10

  rows = [] // rows we display to user
  filteredRows = [] // position specific and search
  searchRows = [] // search specific
  backendData = [] // hold data in memory (prevents slowdown on display)

  // please leave these alone, used for internal tracking
  total = 0
  page = 1

  goToPage(n: number): void {
    this.page = n
    this.update()
  }

  onNext(): void {
    this.page++
    this.update()
  }

  onPrev(): void {
    this.page--
    this.update()
  }

  select(modal, player): void {
    this.selectedPlayer = player
    this.modalService.open(modal)
  }

  debug() {
    console.debug('page:', this.page)
    console.debug('limit:', this.limit)
    console.debug('rows:', this.rows)
    console.debug('players:', this.players)
    console.debug('backend:', this.backendData)
  }

  add(player: Player) {
    this.details = this.draft.add(player)
    // this.details = this.draft.getDetails()
    // console.debug(this.details)
    this.update()
  }

  search(evt) {
    this.searchValue = evt.target.value
    if(!this.searchValue || this.searchValue.length < 1) {
      this.isSearching = false
      this.combineRows()
      return
    }
    this.isSearching = true
    // this.doSearch()
    this.combineRows()
  }

  selectPosition(positionIndex) {
    this.currentPosition = positionIndex
    this.combineRows()
  }

  parseRows(rows) {
    if(!this.details.players) this.details.players = rows
    for (var i = 0; i < rows.length; ++i) {
      let row = rows[i]
      if(!row.esbid) {
        row.esbid = row.teamAbbr
      }

      let playerIndex = this.details.players.findIndex(function(players) { return players.player == row.id; } )
      if(playerIndex > -1) row.taken = true
      
      row.image = '/download/' + row.esbid + '.png'
    }
    this.total = rows.length
    return rows.splice( (this.page - 1) * this.limit, this.limit) // serve array depending on page
  }

  combineRows() {

    this.page = 1

    let rows = [] // combination of filter/search rows

    this.filteredRows = []

    this.position = this.positions.list[this.currentPosition]    

    // position filter
    for (var i = 0; i < this.backendData.length; ++i) {
      let row = this.backendData[i]
      if(!this.position) {
        this.filteredRows.push(row)
        continue
      }
      if(row.position === this.position) {
        this.filteredRows.push(row)
      }
    }

    // search
    let query = new RegExp(this.searchValue, 'gi')
    for (var j = 0; j < this.filteredRows.length; ++j) {
      let row = this.filteredRows[j]
      if(row.name.match(query)) {
        rows.push(row)
      }
    }

    this.filteredRows = rows

    this.update()
  }

  update() {
    let rows = this.backendData.slice() // get all rows
    if(this.filteredRows.length > 0 || this.isSearching) rows = this.filteredRows.slice()
    this.rows = this.parseRows(rows)
    // this.debug()
  }

  ngOnInit() {

    if(!this.players) {
      console.error('No players found for list component')
      return
    }
    
    this.backendData = this.players
    this.total = this.players.length
    
    this.combineRows()

  }

  constructor(private router: Router, private modalService: NgbModal, private draft: DraftComponent) { this.positions = new Positions() }
  
}
