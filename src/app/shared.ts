export class Fantasy {
  week = 14
  TESTPOS = ['QB'] // test positions
  SPOS = ['QB', 'RB', 'WR', 'TE', 'K', 'DEF'] // standard positions
  PPRPOS = [ 'RB-ppr', 'WR-ppr', 'TE-ppr'] // PPR positions
}

export interface Player {
  id: number
  esbid: string
  name: string
  position: string
  image: string
}

export class User {
  constructor() {}
  id: string
  user: string
  pass: string
  email: string
}

export interface Item {}
export interface Stats {}
export interface Draft {}
export interface Rank {}

export class Positions {
  
  list: any

  offense: any
  defense: any

  flex: any

  constructor() {
    this.list = ['QB', 'RB', 'WR', 'TE', 'FLEX', 'K', 'DEF']
    this.offense = ['QB', 'RB', 'WR', 'TE', 'FLEX', 'K']
    this.defense = ['DEF']
    this.flex = ['RB', 'WR']
  }
}