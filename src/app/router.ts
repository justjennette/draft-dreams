import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { BrowserModule } from '@angular/platform-browser'
import { FormsModule } from '@angular/forms'

import { PlayersComponent } from './components/players/players.component'
import { DraftComponent } from './components/draft/draft.component'
import { LoginComponent } from './components/login/login.component'
import { DashboardComponent } from './components/dashboard/dashboard.component'
import { Admin } from './admin/admin.component'

export const routes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  },
  {
    path: 'players',
    component: PlayersComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'draft',
    component: DraftComponent
  },
  {
    path: 'draft/:id',
    component: DraftComponent
  },
  {
    path: 'invite/:invite',
    component: DraftComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'logout',
    component: LoginComponent
  },
  {
    path: 'admin',
    component: Admin
  },
  { 
    path: '**', 
    redirectTo: 'dashboard' 
  }
]

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}