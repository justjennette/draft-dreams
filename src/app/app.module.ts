import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './router';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http'
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { CookieModule } from 'ngx-cookie';

import { API } from './api.service'

import { SafeHtml, ArraySortPipe } from './tools/pipe';
import { ListComponent } from './components/list/list.component';
import { PlayersComponent } from './components/players/players.component'
import { DashboardComponent } from './components/dashboard/dashboard.component'
import { LoginComponent } from './components/login/login.component'
import { PaginationComponent } from './components/pagination/pagination.component';
import { Admin } from './admin/admin.component';
import { NavComponent } from './components/nav/nav.component';
import { DraftComponent } from './components/draft/draft.component';

@NgModule({
  declarations: [
    AppComponent,
    SafeHtml,
    ArraySortPipe,
    PlayersComponent,
    DashboardComponent,
    LoginComponent,
    ListComponent,
    PaginationComponent,
    Admin,
    NavComponent,
    DraftComponent
  ],
  imports: [
    HttpClientModule,
    CookieModule.forRoot(),
    AngularFirestoreModule,
    NgbModule.forRoot(),
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase)
  ],
  providers: [ API, AngularFireDatabase, DraftComponent ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
