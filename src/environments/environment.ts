// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
    production: false,
    firebase: {
        apiKey: 'AIzaSyBbGEbNUCQ4P8SmNWNvdPjyjnfF0q0SyuI',
        authDomain: 'draft-dream.firebaseapp.com',
        databaseURL: 'https://draft-dream.firebaseio.com',
        projectId: 'draft-dream',
        storageBucket: 'draft-dream.appspot.com',
        messagingSenderId: '792076541655'
    },
    urls: {
        base: 'http://api.fantasy.nfl.com/v1', 
        ext: '?format=json&callback='
    }
    // urls: {
    //   base: '/api/json/', 
    //   ext:  '.json'
    // }
};