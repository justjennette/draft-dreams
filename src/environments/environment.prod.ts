export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyBbGEbNUCQ4P8SmNWNvdPjyjnfF0q0SyuI',
    authDomain: 'draft-dream.firebaseapp.com',
    databaseURL: 'https://draft-dream.firebaseio.com',
    projectId: 'draft-dream',
    storageBucket: 'draft-dream.appspot.com',
    messagingSenderId: '792076541655'
  },
  urls: {
    base: 'http://api.fantasy.nfl.com/v1', 
    ext: '?format=json&callback='
  }
};
